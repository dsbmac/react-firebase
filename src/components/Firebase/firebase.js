import app from "firebase/app";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyDjYHQWm1q9zkxR3bAM18ZYyBt3wZUzjCs",
  authDomain: "react-firebase-f1903.firebaseapp.com",
  databaseURL: "https://react-firebase-f1903.firebaseio.com",
  projectId: "react-firebase-f1903",
  storageBucket: "react-firebase-f1903.appspot.com",
  messagingSenderId: "552141983351"
};

class Firebase {
  constructor() {
    app.initializeApp(config);
  }
}

export default Firebase;
